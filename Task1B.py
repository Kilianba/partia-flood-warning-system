from floodsystem.geo import stations_by_distance
from floodsystem.stationdata import build_station_list

def run():
    """Requirements for 1B"""
    station_list = build_station_list()
    # Creates a list of all the stations

    stations_distance = stations_by_distance(station_list, (52.2053, 0.1218)) 
    # Creates a list of tuples with the distance to the cambridge city center
    names_towns_distance_closest10 = []
    names_towns_distance_furthest10 = []

    for k in stations_distance[:10]:
        names_towns_distance_closest10.append((k[0].name, k[0].town, k[1]))
        # Adds the name, the town and the distance of the station as a tuple
    for k in stations_distance[-10:]:
        names_towns_distance_furthest10.append((k[0].name, k[0].town, k[1]))

    print(names_towns_distance_closest10)
    print(names_towns_distance_furthest10)

if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")
    run()


