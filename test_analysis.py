from floodsystem.analysis import polyfit

from datetime import datetime
import numpy as np

def test_polyfit():
	dates = [
		datetime(year = 1970, month = 1, day = 1),
		datetime(year = 1970, month = 1, day = 2),
		datetime(year = 1970, month = 1, day = 3)
	]
	levels = [1, 4, 1]
	poly, t0 = polyfit(dates, levels, 2)
	assert abs(poly[0] - 1) < 0.000001
	assert abs(poly[1] - 6) < 0.000001
	assert abs(poly[2] + 3) < 0.000001
	assert t0 == 719163  # 1 Jan 1970
