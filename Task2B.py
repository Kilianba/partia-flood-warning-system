from floodsystem.flood import stations_level_over_threshold
from floodsystem.stationdata import build_station_list, update_water_levels


def run():
     # Build list of stations
    stations = build_station_list()

    # Update latest level data for all stations
    update_water_levels(stations)

    out = ""

    for pair in stations_level_over_threshold(stations, 0.8):
        out += pair[0].name + ": " + str(pair[1]) + "\n"

    print(out)


if __name__ == "__main__":
    print("*** Task 2B: CUED Part IA Flood Warning System ***")
    run()
