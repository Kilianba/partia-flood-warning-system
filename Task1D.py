from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.stationdata import build_station_list

def run():
    """Requirements for 1D"""
    station_list = build_station_list()
    #Creates a list of all the stations

    rivers = rivers_with_station(station_list)
    print("There are {} rivers with a station on it".format(len(rivers)))

    print(sorted(rivers)[:10])

    dict_rivers_to_stations = stations_by_river(station_list)

    for river in ["River Aire", "River Cam", "River Thames"]:
        names_on_k = [x.name for x in dict_rivers_to_stations[river]]
        print("On the {} there are the following monitoring stations: ".format(river))
        print(sorted(names_on_k))

if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")
    run()