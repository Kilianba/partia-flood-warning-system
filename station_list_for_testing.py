from floodsystem.station import MonitoringStation

stations = [
	MonitoringStation("http://environment.data.gov.uk/flood-monitoring/id/stations/1029TH", "http://environment.data.gov.uk/flood-monitoring/id/measures/1029TH-level-stage-i-15_min-mASD", "Bourton Dickler", (51.874767, -1.740083), (0.068, 0.42), "River Dikler", "Little Rissington"),
	MonitoringStation("http://environment.data.gov.uk/flood-monitoring/id/stations/E2043", "http://environment.data.gov.uk/flood-monitoring/id/measures/E2043-level-stage-i-15_min-mASD", "Surfleet Sluice", (52.845991, -0.100848), (0.15, 0.895), "River Glen", "Surfleet Seas End"),
	MonitoringStation("http://environment.data.gov.uk/flood-monitoring/id/stations/52119", "http://environment.data.gov.uk/flood-monitoring/id/measures/52119-level-stage-i-15_min-mASD", "Gaw Bridge", (50.976043, -2.793549), (0.231, 3.95), "River Parrett", "Kingsbury Episcopi")
]

stations[0].latest_level = 0.43
stations[1].latest_level = 0.8
stations[2].latest_level = 0.231
