import numpy as np
import matplotlib.dates as mpdate

def polyfit(dates, levels, p):
	"""Fit the water level and date data to a polynomial of degree p, and return it as a numpy poly1d.
	The dates are converted to days since year 0, and timeshifted such that the initial value is at 0. The amount of time shifting is returned as well."""
	if len(dates) > 0:
		dates = mpdate.date2num(dates)
		return np.poly1d(np.polyfit(dates - dates[0], levels, p)), dates[0]
	else:  # If no level/date data was fetched
		return np.poly1d(0), 0
