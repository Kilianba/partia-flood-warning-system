""" This Module contains a collection of modules related to floods"""

from .utils import sorted_by_key


def stations_level_over_threshold(stations,tol):
    """ Returns a list of tuples of the MonitoringStations in the 
    list stations at which the relative water level is over tol.
    The highest relative water levels come first."""

    out =[]

    for station in stations:
        
        if station.typical_range_consistent() and station.latest_level is not None:
            
            if station.relative_water_level() > tol:
                
                out += [(station , station.relative_water_level() )]
    
    return sorted_by_key(out,1,True)

def stations_highest_rel_level(stations,N):
    """Returns the N MonitoringStations with the highest relative water level. Output is in descending order. """
    out =[]

    for station in stations:
        
        if station.typical_range_consistent() and station.latest_level is not None:
            
            out += [(station , station.relative_water_level() )]
    
    sort_tups = sorted_by_key(out,1,True)

    return [ k[0] for k in sort_tups[:N] ]   
