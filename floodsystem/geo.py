# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.
"""

from .utils import sorted_by_key  # noqa

import math as m

def haversine(x_1, x_2):
    """ Takes two geographic coordinates in the form of (latitude, longitude) and 
    returns the distance between them in kilometres using the haversine formula,
    taking the radius of the earth to be 6364.9 km, as approximated at latitude 52 in Cambridge"""
    r = 6364.9
    # a, b = x_1, x_2 in radians
    a = [x_1[0] * m.pi / 180, x_1[1] * m.pi / 180] #transforms degrees into radians
    b = [x_2[0] * m.pi / 180, x_2[1] * m.pi / 180]
    c = (m.sin((b[0] - a[0]) / 2))**2 + m.cos(a[0]) * m.cos(b[0]) * (m.sin((b[1] - a[1]) / 2)**2)

    return 2 * r * m.asin(m.sqrt(c))


def stations_by_distance(stations, p):
    """Returns a list of tuples containing stations and 
    their distance to the coordinate p in kilometres"""

    tuples = []

    for station in stations:
        tuples += [(station, haversine(p, station.coord))]

    return sorted_by_key(tuples, 1)
    
def stations_within_radius(stations, centre, r):
    """Returns a list of stations within a radius (in km) of a coordinate given as (latitude, longitude)"""

    final_stations = []

    for station in stations:
        if haversine(centre, station.coord) <= r:
            final_stations.append(station)

    return final_stations

    
def rivers_with_station(stations):
    """Returns a set of rivers with at least one station from the input list"""

    rivers = set(x.river for x in stations)
    return rivers
    
    
def stations_by_river(stations):
    """Returns a dictionary that maps river names to a list of stations on the given river"""

    riverstations = {}

    for station in stations:
        if not station.river in riverstations:
            riverstations[station.river] = []
        riverstations[station.river].append(station)

    return riverstations

def rivers_by_station_number(stations, N):
    """Returns a list of tuples of river names and the number of stations on it"""

    riverstations = stations_by_river(stations)
    rivernumbers = []
    for river in riverstations:
        rivernumbers.append((river, len(riverstations[river])))
    rivernumbers = sorted_by_key(rivernumbers, 1, True)

    out = rivernumbers[:N] # N rivers with most stations on it
    tie = out[-1][1] # the amount of stations on the Nth ranked river
    i = N
    while i < len(rivernumbers) and rivernumbers[i][1] == tie:
        #Makes sure that if there is a tie at the end, all rivers are accounted for
        out.append((rivernumbers[i]))
        i += 1

    return out
