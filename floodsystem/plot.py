from floodsystem.analysis import polyfit

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mpdate
from datetime import datetime, timedelta

def plot_water_levels(station, dates, levels):
	""" Plot of water level against time for the given station, with typical low and high levels shown """

	# Plot
	plt.plot(dates, levels, '.', label = "water level")

	# Thresholds
	l = len(dates)
	plt.plot(dates, [station.typical_range[0]] * l, label = "typical low")
	plt.plot(dates, [station.typical_range[1]] * l, label = "typical high")

	# Add axis labels, rotate date labels and add plot title
	plt.xlabel('Date/time')
	plt.ylabel('Water level (m)')
	plt.xticks(rotation = 45)
	plt.title(station.name)
	plt.legend()

	# Display plot
	plt.tight_layout()  # This makes sure plot does not cut off date labels

	plt.show()

def plot_water_levels_with_fit(station, dates, levels, p):
	""" Plot of water level against time for the given station with a fitted polynomial of degree p, and with typical low and high levels shown """

	# Plot
	plt.plot(dates, levels)

	# Thresholds
	l = len(dates)
	plt.plot(dates, [station.typical_range[0]] * l)
	plt.plot(dates, [station.typical_range[1]] * l)

	# Fit
	poly, t0 = polyfit(dates, levels, p)
	plt.plot(dates, poly(mpdate.date2num(dates) - t0))

	# Add axis labels, rotate date labels and add plot title
	plt.xlabel('Date/time')
	plt.ylabel('Water level (m)')
	plt.xticks(rotation = 45)
	plt.title(station.name)

	# Display plot
	plt.tight_layout()  # This makes sure plot does not cut off date labels

	plt.show()
