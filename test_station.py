# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""Unit test for the station module"""

from floodsystem.station import MonitoringStation, inconsistent_typical_range_stations
from station_list_for_testing import stations

def test_create_monitoring_station():

    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town

def test_inconsistent_typical_range_stations():
    inconsistents = inconsistent_typical_range_stations(stations)
    for station in inconsistents:
        assert station.typical_range is None or station.typical_range[0] > station.typical_range[1]

def test_relative_water_level():
    station = stations[0]
    station.latest_level = 0.068
    assert abs(station.relative_water_level() - 0) < 0.000001
    station.latest_level = 0.42
    assert abs(station.relative_water_level() - 1) < 0.000001
    station.latest_level = 0.244
    assert abs(station.relative_water_level() - 0.5) < 0.000001
