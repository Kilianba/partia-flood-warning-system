from floodsystem.geo import stations_within_radius
from floodsystem.stationdata import build_station_list

def run():
    """Requirements for 1C"""
    station_list = build_station_list()
    #Creates a list of all the stations

    stations_10k_to_city_center = stations_within_radius(station_list, (52.2053, 0.1218), 10) 
    # Creates a list of stations within 10km of the cambridge city center
    names = []
    for k in stations_10k_to_city_center:
        names.append(k.name)
    
    print(sorted(names))

if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")
    run()