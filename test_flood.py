from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level
from station_list_for_testing import stations

def test_stations_level_over_threshold():
	assert len(stations_level_over_threshold(stations, 1.1)) == 0
	assert len(stations_level_over_threshold(stations, 1)) == 1
	assert len(stations_level_over_threshold(stations, 0.8)) == 2
	l = stations_level_over_threshold(stations, 0)
	assert len(l) == 2
	assert l[0][1] > l[1][1]

def test_stations_highest_rel_level():
	for i in range(4):
		l = stations_highest_rel_level(stations, i)
		assert len(l) == i
		for j in range(i - 1):
			assert l[j].relative_water_level() > l[j + 1].relative_water_level()
