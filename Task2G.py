from floodsystem.stationdata import build_station_list, update_water_levels
import floodsystem.datafetcher as datafetcher
from floodsystem.analysis import polyfit

import datetime
import numpy as np
import matplotlib.dates as mpdate

def flood_warning(stations, dt):
	"""Evaluates a list of stations to determine their flood warning levels, based on current levels as well as predicted future level after time dt, and prints it to console.
	The prediction of future levels can be disabled (for performance reasons) by setting dt to 0.
	"""

	# Warning levels:
	# 0 - none
	# 1 - low
	# 2 - moderate
	# 3 - high
	# 4 - severe

	stations = [[s, 0] for s in stations]  # Initialise warning level to 0 for all stations

	for pair in stations:
		station = pair[0]
		rel = station.relative_water_level()

		if rel is not None:
			dates, levels = datafetcher.fetch_measure_levels(station.measure_id, datetime.timedelta(days = 2))
			if dt == 0:
				poly = np.poly1d(0)
			else:
				poly, t0 = polyfit(dates, levels, 4)

			if poly == np.poly1d(0):  # If no level/date data was fetched, or if dt is 0
				der = np.poly1d(0)
				rate = 0
			else:
				der = np.polyder(poly)
				rate = der(mpdate.date2num(dates[-1]) - t0)  # minus t0 to account for date adjustment in polyfit

			future = rel + rate * dt

			if rel > 3:
				pair[1] = 4
			elif rel > 2:
				if future > 1:
					pair[1] = 4
				else:
					pair[1] = 3
			elif rel > 1.5:
				if future > 1:
					pair[1] = 3
				else:
					pair[1] = 2
			elif rel > 1.2:
				if future > 1:
					pair[1] = 2
				else:
					pair[1] = 1
			elif rel > 1:
				pair[1] = 1

			if future > 2:
				pair[1] = 4
			elif future > 1.5:
				if pair[1] < 3:
					pair[1] = 3
			elif future > 1.2:
				if pair[1] < 2:
					pair[1] = 2
			elif future > 1:
				if pair[1] < 1:
					pair[1] = 1

		if pair[1] == 0:
			pair.append("none")
		elif pair[1] == 1:
			pair.append("low")
		elif pair[1] == 2:
			pair.append("moderate")
		elif pair[1] == 3:
			pair.append("high")
		elif pair[1] == 4:
			pair.append("severe")

		if pair[1] > 0:
			print(pair[0].name + ": " + pair[-1])

	return stations

def run():
	stations = build_station_list()
	update_water_levels(stations)
	flood_warning(stations, 0.1)

if __name__ == "__main__":
	print("*** Task 2G: CUED Part IA Flood Warning System ***")
	run()
