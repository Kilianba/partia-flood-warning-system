from floodsystem.geo import haversine, stations_by_distance, stations_within_radius, rivers_with_station, stations_by_river, rivers_by_station_number
from station_list_for_testing import stations

def test_haversine():
	assert abs(haversine((180, 0), (0, 0)) - 20020) <= 20020 * 0.01
	assert abs(haversine((50, 5), (58, 3)) - 899) <= 899 * 0.01
	assert abs(haversine((73, 110), (2, -20)) - 11000) <= 11000 * 0.01

def test_stations_by_distance():
	p = (52.2053, 0.1218)
	stations_distance = stations_by_distance(stations, p)
	for i in range(len(stations_distance) - 1):
		assert stations_distance[i][1] <= stations_distance[i + 1][1]

def test_stations_within_radius():
	p = (52.2053, 0.1218)
	r = 21
	stations_within = stations_within_radius(stations, p, r)
	for station in stations_within:
		assert haversine(p, station.coord) <= r

def test_rivers_with_station():
	rivers = rivers_with_station(stations)
	for station in stations:
		assert station.river in rivers
	for station in stations:
		if station.river in rivers:
			rivers.remove(station.river)
	assert len(rivers) == 0

def test_stations_by_river():
	riverstations = stations_by_river(stations)
	for station in stations:
		assert station in riverstations[station.river]

def test_rivers_by_station_number():
	n = 10
	rivernumbers = rivers_by_station_number(stations, n)
	if len(stations) >= n:
		assert len(rivernumbers) >= n
	else:
		assert len(rivernumbers) == len(stations)

	for i in range(len(rivernumbers) - 1):
		assert rivernumbers[i][1] >= rivernumbers[i + 1][1]

	if i in range(n, len(rivernumbers)):
		assert rivernumbers[i][1] == rivernumbers[n - 1]


